const alert = document.getElementById('alert');
const submitButton = document.getElementById('covert-button')

const youtube_parser = (url) => {
    var regExp = /(?:youtube(?:-nocookie)?\.com\/(?:[^\/\n\s]+\/\S+\/|(?:v|e(?:mbed)?)\/|\S*?[?&]v=)|youtu\.be\/)([a-zA-Z0-9_-]{11})/;
    return (url.match(regExp)[1])
}

const validVideoId = (id) => {
    var img = new Image();
    img.src = "http://img.youtube.com/vi/" + id + "/mqdefault.jpg";
    img.onload = function () {

        checkThumbnail(this.width);
    }
}

const checkThumbnail = (width) => {
    if (width === 120) {
        errorAlert("Video does not exist");
    }

}
const errorAlert = (info) => {
    alert.innerHTML = `<div class="alert alert-danger" role="alert">${info}</div>`
    setTimeout(() => {
        alert.innerHTML = ''
    }, 3000)
}


const convert = (e) => {


    try {
        let current = window.location.href;
        console.log(current);
        const url = document.querySelector('.form-control').value;
        const id = youtube_parser(url);
        validVideoId(id);
        submitButton.disabled = true;
        window.location.href = `${current}download?URL=${url}`
        // console.log(url);

        // const id = youtube_parser(url);
        // validVideoId(id);
        //Send request
        // submitButton.setAttribute('disabled');

    } catch{
        e.preventDefault();
        document.getElementById('spinner').innerHTML = ``;
        errorAlert('Invalid YouTube URL');
    }
}

submitButton.addEventListener('click', convert);
