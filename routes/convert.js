const path = require('path');
const fs = require('fs');

const express = require('express');
const ytdl = require('ytdl-core');
const getYoutubeTitle = require('get-youtube-title')

const app = express();

const router = express.Router();


const extractID = (url) => {
    let regExp = /(?:youtube(?:-nocookie)?\.com\/(?:[^\/\n\s]+\/\S+\/|(?:v|e(?:mbed)?)\/|\S*?[?&]v=)|youtu\.be\/)([a-zA-Z0-9_-]{11})/;
    return (url.match(regExp)[1])
}

// async function deleteFile(file) {
//     await setTimeout(() => {
//         fs.unlink(file);
//         console.log('File Deleted')
//     }, 1000) // delete file timeout
// }

router.use("/download", (req, res, next) => {
    const { URL } = req.query;
    const id = extractID(URL);
    const API_KEY = process.env.API_KEY || "AIzaSyBqirYorhZZjirI_6ixiI_8Sy1b-ANhcp0";
    getYoutubeTitle(id, API_KEY, (err, title) => {
        if(err){
            console.log(err);
            res.redirect("/");
        }
        res.setHeader('Content-Type', 'audio/mpeg');
        res.header('Content-Disposition', `attachment; filename="${title}.mp3"`);
        ytdl(URL, { format: 'mp3' }).pipe(res);
        console.log(`Finished: ${title}`);
    })
})
module.exports = router;








